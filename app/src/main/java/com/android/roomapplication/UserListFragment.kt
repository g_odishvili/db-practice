package com.android.roomapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.roomapplication.databinding.FragmentUserListBinding
import com.android.roomapplication.databinding.LandingFragmentBinding
import com.android.roomapplication.model.User


class UserListFragment : Fragment() {
    private lateinit var binding: FragmentUserListBinding
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUserListBinding.inflate(
            inflater, container, false
        )
        init()
        return binding.root
    }

    private fun init() {
        adapter = RecyclerViewAdapter()
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        binding.recyclerView.adapter = adapter
        setData()
    }

    private fun setData() {
        adapter.setData(arguments?.get("users") as List<User>)
    }

}