package com.android.roomapplication

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.roomapplication.databinding.ItemUserLayoutBinding
import com.android.roomapplication.model.User

class RecyclerViewAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val userList: MutableList<User> = mutableListOf()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return UserViewHolder(
            ItemUserLayoutBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is UserViewHolder) {
            holder.bind()
        }
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    inner class UserViewHolder(private val binding: ItemUserLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var user: User

        fun bind() {
            user = userList[adapterPosition]
            binding.tvFirstName.text = user.firstName
            binding.tvLastName.text = user.lastName
            binding.tvId.text = user.id.toString()
        }

    }

    fun setData(list: List<User>){
        this.userList.clear()
        this.userList.addAll(list)
        notifyDataSetChanged()
    }
}

