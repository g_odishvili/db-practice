package com.android.roomapplication.dao

import androidx.lifecycle.LiveData
import com.android.roomapplication.model.User

class UserRepository(private val userDao: UserDao) {
    val readAllData: LiveData<List<User>> = userDao.readAllData()

    suspend fun addUser(user: User) {
        userDao.addUser(user)
    }
}