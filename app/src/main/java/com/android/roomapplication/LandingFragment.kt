package com.android.roomapplication

import android.app.Dialog
import android.os.Bundle
import android.util.Log.i
import android.view.*
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.android.roomapplication.databinding.ItemDialogLayoutBinding
import com.android.roomapplication.databinding.LandingFragmentBinding
import com.android.roomapplication.extensions.setUp
import com.android.roomapplication.model.User


class LandingFragment : Fragment() {


    private val viewModel: LandingViewModel by viewModels()
    private lateinit var binding: LandingFragmentBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = LandingFragmentBinding.inflate(
            inflater, container, false
        )
        init()
        observes()
        return binding.root
    }

    private fun init() {
        binding.btnSave.setOnClickListener {
            addUser()
        }
        binding.btnRead.setOnClickListener {
            readUsers()
        }

        binding.btnError.setOnClickListener {
            showError()
        }
    }

    private fun showError() {
        val dialog = Dialog(requireContext())
        val dialogBinding = ItemDialogLayoutBinding.inflate(layoutInflater)
        dialog.setUp(dialogBinding,R.color.design_default_color_error,WindowManager.LayoutParams.WRAP_CONTENT)
        dialogBinding.closeButton.setOnClickListener {
            dialog.cancel()
        }
        dialog.show()

    }

    private fun readUsers() {
        view?.findNavController()
            ?.navigate(R.id.action_landingFragment2_to_userListFragment, bundleOf("users" to viewModel.readAllData.value))
    }

    private fun observes() {
        viewModel.readAllData.observe(viewLifecycleOwner, {
            i("DebugSIZE",it.size.toString())
        })
    }

    private fun addUser() {
        val user = User(
            binding.firstName.text.toString(),
            binding.lastName.text.toString(),
            binding.age.text.toString().toIntOrNull(),
            binding.address.text.toString(),
            binding.height.text.toString().toIntOrNull(),
            ""
        )
        viewModel.addUser(user)

        binding.firstName.setText("")
        binding.lastName.setText("")
        binding.age.setText("")
        binding.address.setText("")
        binding.height.setText("")
    }

}